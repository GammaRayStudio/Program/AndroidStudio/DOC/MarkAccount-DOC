Android App 開發 : 用 Markdown 記帳的 App
======
`2021-11-03`

目前進度
------

    結合 FileUtil 與 RecyclerView，
    實作 「檔案瀏覽器」的功能。
    
    今日任務:
    • 實作 檔案動作列表 (滑鼠的右鍵) 
    • 調整 懸浮按鈕的功能選單 (增加動畫與選單)
    • 佈局完成後，剩餘時間，實作檔案功能動作


+ [完整進度: to-do]
[完整進度: to-do]:https://gitlab.com/GammaRayStudio/Program/AndroidStudio/DOC/MarkAccount-DOC/-/blob/master/001.mark-account-to_do.md

### 調整
+ 半夜時分，有空的時候就會來開發
+ 實況的時候，在稍微講一下實作內容與進度

<br>

今日目標
------
+ 於 AndroidProjUI 專案 測試 Recycler View 元件
+ 撰寫 檔案瀏覽器 畫面佈局的規格 `功能與細節`

<br>


產品程式
------
`若牽涉到核心隱私的實作，會再視情況獨立成 private 的專案`

### MarkAccount 
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/APP/MarkAccount>

<br>


開發文件
------
### MarkAccount-DOC
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/DOC/MarkAccount-DOC>

<br>


工具專案
------
### JavaProjUtil 
<https://gitlab.com/GammaRayStudio/Program/JavaStudio/SE/JavaProjUtil>


### 工具專案 : AndroidProjUI
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/SE/AndroidProjUI>

### 工具專案 : AndroidFileBrowser
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/SE/AndroidFileBrowser>



