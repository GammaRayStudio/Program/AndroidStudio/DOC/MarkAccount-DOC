開發日誌
======


2021-10-02 , Sat
------
`#001`

```
【開發日誌】
1. 流程規劃 :
    1.1. 待辦清單
    1.2. 文件規格
2. 準備工具:
    2.1.  Android Studio 快捷鍵 配置
    2.2. Android Studio 插件 安裝
3. 開始開發
    3.1. 創一個全新的專案，安裝在實體手機上
    3.2. 更改 App ICON 直接卡關，已經解決
    3.2. 開始實做第一個部分，檔案瀏覽器，已創建工具專案

【GitLab 專案】
開發文件 : MarkAccount-DOC
https://gitlab.com/GammaRayStudio/Program/AndroidStudio/DOC/MarkAccount-DOC

產品程式 : MarkAccount
https://gitlab.com/GammaRayStudio/Program/AndroidStudio/APP/MarkAccount

工具專案 : JavaProjUtil
https://gitlab.com/GammaRayStudio/Program/JavaStudio/SE/JavaProjUtil
```

<br>

2021-10-03 , Sun
------
`#002-1 , #002-2`

```
上半部分 : 002-1
實作 JavaProjUtil 工具專案內容
1. FileUtil.java - 檔案處理工具
2. FileUtilTest.java - FileUtil 測試

( 21:30 有事外出，22:30 左右繼續開發) 

下半部分 : 002-2

FileUtil 繼續實作，剩餘三個接口的功能與測試尚未實作:
1. copy
2. delete
3. content

(進度更新在 GitLab 的開發文件 #20211004001 )
```


2021-10-06 , Wed
------
`#003`

```
工具專案 JavaProjUtil :
1. FileUtil.java - 檔案處理工具
2. FileUtilTest.java - FileUtil 測試

兩者實作完成，並且提交推送


Android 產品專案，導入 FileUtil 測試:

已確認可以在 sdcard 的目錄下，讀寫文檔
```


2021-10-07 , Thur
-----
`#004`

```
創建 AndroidProjUI 專案，用於測試 Recycler View 元件
創建 AndroidFileBrowser 專案，用於實作「檔案瀏覽器」的功能

用神秘的 Google 力量，初步測試 Recycler View 元件，
AndroidProjUI 的主頁面已導入，但點擊事件還有些問題
```


